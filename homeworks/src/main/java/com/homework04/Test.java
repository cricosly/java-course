package com.homework04;

public class Test {
    public static void main(String[] args) {
        Student s1 = new Student("Wu", Student.Sex.FEMALE);
        Student s2 = new Student("Wang", Student.Sex.MALE);
        System.out.println(s1.getName());
        System.out.println(s1.getSex());
        System.out.println(s2.getName());
        System.out.println(s2.getSex());
    }
}
