package com.homework04;

public class Student {
    //枚举性别使其合理
    enum Sex {
        MALE,FEMALE
    }
    private String name;
    private Sex sex;

    //两参数构造函数
    public Student(String name, Sex sex) {
        this.name = name;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}
