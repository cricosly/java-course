package com.p10IO;

import java.io.*;

public class Test2 {
    public static void main(String[] args) throws IOException {
        String inputFile = "D:/input.txt";
        String outputFile = "D:/output.txt";
        copyFile(inputFile, outputFile);
    }
    private static void copyFile(String inputFile, String outputFile) {
        try (InputStream in = new FileInputStream(inputFile);
             OutputStream out = new FileOutputStream(outputFile)) {
            int size = 4;
            byte[] buffer = new byte[size];
            int len;
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            System.out.println("复制成功");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
/*编写一个方法，传入输入源文件路径，以及输出源文件路径。路径为包含了文件路径/名称/扩展名，的字符串描述
//OutputStream out = new FileOutputStream(outputFile)在文件不存在时自动创建目标文件
可以是文本文件或任何类型文件

方法实现：基于基本字节数组缓冲区的IO流操作，基于指定输入源/输出源复制文件；资源必须被正确关闭；异常在方法内处理；无需考虑目录不存在问题*/