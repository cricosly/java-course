package com.p10IO;

import java.io.*;

public class Test{

    public static void copyFile(String inputFilePath, String outputFilePath) throws IOException {
        InputStream input = null;
        OutputStream output = null;
        try {
            // 创建输入流和输出流
            input = new FileInputStream(inputFilePath);
            output = new FileOutputStream(outputFilePath);

            // 定义缓冲区大小
            byte[] buffer = new byte[1024];
            int bytesRead;

            // 使用while循环读取数据并写入输出流
            while ((bytesRead = input.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } finally {
            // 无论如何都需要关闭流
            if (input != null) {
                input.close();
            }
            if (output != null) {
                output.close();
            }
        }
    }

}
