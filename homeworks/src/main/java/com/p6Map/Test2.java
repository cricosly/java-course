package com.p6Map;

import java.util.*;

public class Test2 {
    private static final Set<User> USERS = create();

    public static void main(String[] args) {
        toMap(USERS);
    }

    private static Set<User> create() {
        User u1 = new User(1, "C1", User.CHENGDU);
        User u2 = new User(2, "C2", User.CHENGDU);
        User u3 = new User(3, "H1", User.HAERBIN);
        User u4 = new User(4, "H2", User.HAERBIN);
        Set<User> users = new HashSet<>();
        users.add(u1);
        users.add(u2);
        users.add(u3);
        users.add(u4);
        return users;
    }

    //基于迭代器
    private static void toMap(Set<User> users) {
        //城市名称为键，用户集合为值
        Map<String, List<User>> map = new HashMap<>();
        for (User u : users) {
            String city = u.getCity();
            //判断Map中是否含有 u所在的城市 这个键
            //若没有对应的城市键
            //添加新键值对，其中键为城市名称，值为创建一个空的用户集合
            if (!map.containsKey(city)) {
                map.put(city, new ArrayList<>());
            }
            //map.get(city)返回city键对应的值（集合）的引用
            //通过集合的add()方法将用户u添加到该集合中
            map.get(city).add(u);
        }

        //entrySet()返回一个保存键值对的set集合，便于遍历的取出数据
        //Map集合通过entrySet()方法转换成set集合
        //set集合中元素的类型是Map.Entry<K,V>
        Set<Map.Entry<String, List<User>>> set = map.entrySet();
        Iterator<Map.Entry<String, List<User>>> iterator = set.iterator();
        while (iterator.hasNext()) {
            //var动态类型是编译器根据变量所赋的值来推断类型
            var mSet = iterator.next();
            String city = mSet.getKey();
            List<User> userList = mSet.getValue();
            System.out.println(city + ": ");
            for (User user : userList) {
                System.out.println(user.getName());
            }
        }
    }
}
