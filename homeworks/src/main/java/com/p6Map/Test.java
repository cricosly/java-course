package com.p6Map;

import java.util.*;

public class Test {
    private static final Set<User> USERS = create();

    public static void main(String[] args) {
        toMap(USERS);
    }

    private static Set<User> create() {
        User u1 = new User(1, "C1", User.CHENGDU);
        User u2 = new User(2, "C2", User.CHENGDU);
        User u3 = new User(3, "H1", User.HAERBIN);
        User u4 = new User(4, "H2", User.HAERBIN);
        Set<User> users = new HashSet<>();
        users.add(u1);
        users.add(u2);
        users.add(u3);
        users.add(u4);
        return users;
    }

    private static void toMap(Set<User> users) {
        //城市名称为键，用户集合为值
        Map<String, List<User>> map = new HashMap<>();
        for (User u : users) {
            String city = u.getCity();
            //判断Map中是否含有 u所在的城市 这个键
            //若没有对应的城市键
            //添加新键值对，其中键为城市名称，值为创建一个空的用户集合
            if (!map.containsKey(city)) {
                map.put(city, new ArrayList<>());
            }
            //map.get(city)返回city键对应的值（集合）的引用
            //通过集合的add()方法将用户u添加到该集合中
            map.get(city).add(u);
        }

        //entrySet()返回一个保存键值对的Set集合，便于遍历的取出数据
        //Map通过entrySet()方法转换成Set集合
        //Set集合中元素的类型是Map.Entry<K,V>
        Set<Map.Entry<String, List<User>>> set = map.entrySet();
        //Set集合支持foreach循环语句遍历
        for (Map.Entry<String, List<User>>  entry : set) {
            //Map.Entry是Map声明的一个内部接口，此接口为泛型，定义为Entry<K,V>。
            //它表示Map中的一个实体（一个key-value对）。
            //接口中有getKey(),getValue方法。

            //获取城市名
            String city = entry.getKey();
            //获取城市对应的用户List集合
            List<User> userList = entry.getValue();
            System.out.println(city + ": ");
            for (User user : userList) {
                System.out.println(user.getName());
            }
        }
    }
}
