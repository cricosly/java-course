package com.homework05;

import com.homework05.entity.Character;
import com.homework05.entity.Game;
import com.homework05.entity.User;

public class Test {
    public static void main(String[] args) {
        User u = createUser();
        listGames(u);

        /*
        System.out.println(u.getId());
        System.out.println(u.getName());
        System.out.println(u.getCharacter().getId());
        System.out.println(u.getCharacter().getName());
        System.out.println(u.getGames().length);
        Game[] games = u.getGames();
        System.out.println(games[0].getId());
        System.out.println(games[0].getName());
        System.out.println(games[1].getId());
        System.out.println(games[1].getName());
        */

    }

    //创建并返回一个用户对象
    public static User createUser() {

        //创建两个角色对象
        Character c1 = new Character();
        c1.setId(1);
        c1.setName("普通用户");
        Character c2 = new Character();
        c2.setId(2);
        c2.setName("VIP");

        //创建两个游戏对象
        Game g1 = new Game();
        g1.setId(1);
        g1.setName("魂斗罗");
        Game g2 = new Game();
        g2.setId(2);
        g2.setName("超级马里奥");

        User u1 = new User();
        u1.setId(1);
        u1.setName("Mike");
        //用户拥有VIP身份
        u1.setCharacter(c2);
        //用户拥有以上两个游戏
        Game[] games = {g1, g2};
        u1.setGames(games);

        return u1;
    }

    //接受一个用户对象，将该用户拥有的全部游戏的名称打印显示
    public static void listGames(User user) {
        for (Game g : user.getGames()) {
            System.out.println(g.getName());
        }
    }
}
