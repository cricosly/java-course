package com.homework06;

public class Test {
    public static void main(String[] args) {
        clickTest();
    }

    public static void clickTest() {

        //创建ButtonA
        Button buttonA = new Button("ButtonA");
        //使用匿名内部类实现ButtonA被点击
        buttonA.setClickable(new Clickable() {
            @Override
            public void click() {
                System.out.println(buttonA.getName());
            }
        });
        //buttonA被点击
        buttonA.getClickable().click();

        //创建ButtonB
        Button buttonB = new Button("ButtonB");
        //使用匿名内部类实现ButtonB被点击
        buttonB.setClickable(new Clickable() {
            @Override
            public void click() {
                System.out.println(buttonB.getName());
            }
        });
        //buttonB被点击
        buttonB.getClickable().click();

    }

}
