package com.homework06;

public class View {
    //将点击的能力作为类的一个拥有的属性
    private Clickable clickable;


    public Clickable getClickable() {
        return clickable;
    }

    public void setClickable(Clickable clickable) {
        this.clickable = clickable;
    }



    /*
    private String name;

    public View(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
     */
}