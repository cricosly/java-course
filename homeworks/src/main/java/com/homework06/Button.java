package com.homework06;

public class Button extends View {
    //Button有名字
    private String name;

    public Button(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
    public Button(String name) {
        super(name);
    }
     */
}
