package com.homework06;

public interface Clickable {
    void click();
}
