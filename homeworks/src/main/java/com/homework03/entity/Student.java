package com.homework03.entity;

public class Student {
    public static final String MALE = "male";
    public static final String FEMALE = "female";
    private int number;
    private String name;
    private String sex;
    private int age;
    //全属性参数的构造函数
    public Student(int number, String name, String sex, int age) {
        this.number = number;
        this.name = name;
        this.sex = sex;
        this.age = age;
    }
    //每个属性编写对应的getter/setter方法
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}
