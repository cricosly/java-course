package com.homework03;

import com.homework03.entity.Student;

public class StudentTest {
    public static void main(String[] args) {
        Student s1 = new Student(2046, "张凡", Student.MALE, 18);
        s1.setNumber(2059);
        System.out.println("学生姓名:" + s1.getName());
        System.out.println("学生性别:" + s1.getSex());
        System.out.println("学生学号:" + s1.getNumber());
        System.out.println("学生年龄:" + s1.getAge());
    }
}
