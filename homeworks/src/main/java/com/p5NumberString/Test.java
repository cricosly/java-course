package com.p5NumberString;

public class Test {
    public static void main(String[] args) {
        //Q1
        int a = 2;
        int b = 5;
        System.out.println("Q1:");
        System.out.println("result: " + a * b);

        //Q1
        //自动装箱
        Integer n1 = 10;  // 1
        //自动拆箱
        int n2 = n1; // 2
        System.out.println("Q2:");
        System.out.println(n1);
        System.out.println(n2);

        //Q3
        int n = Integer.parseInt("2046");
        System.out.println("Q3:");
        System.out.println(n);
        System.out.println(getType(n));
        //int n = String.parseInt("2046");
        //int n = ("2046").toInt();
        //int n = ("2046").intValue();

        //Q4
        System.out.println("Q4:");
        double balance = 15.5;
        double price = 4.5;
        System.out.println(Math.floor(balance / price));

        //Q5
        System.out.println("Q5:");
        String str1 = new String("welcome");
        String str2 = new String("welcome");
        System.out.println(	str1 == str2);//false
        System.out.println(str1.equals(str2));//true
        System.out.println(str1 = str2);//welcome
        //System.out.println(str1.length(str2));//报错

        //Q7
        System.out.println("Q7:");
        System.out.println(readPathTest("/courses/lecture/Lecture01.pdf"));

        //Q8
        System.out.println();
        System.out.println("Q8:");
        randomTest(1,20,10);


    }
    public static String getType(Object obj){
        return obj.getClass().toString();
    }

    private static String readPathTest(String path) {
        //去掉前后空格
        path.trim();
        //从后往前检索的第一个“/”之后为文件名
        int startIndex = path.lastIndexOf("/") + 1;
        //从后往前检索的第一个“.”之前为文件名
        int endIndex = path.lastIndexOf(".");
        return path.substring(startIndex, endIndex);
    }

    private static void randomTest(int start, int end, int times) {
        if (start >= end || times <= 0) {
            System.out.println("Error!");
            return;
        }
        for (int i = 0; i < times; i++) {
            //start <= x < end
            int x = start + (int) (Math.random() * (end - start));
            System.out.println(x);
        }
    }

}
