package com.hidename;

//让student2类直接接入接口Playable
//必须在类中具体实现抽象的方法“sing”，在第8行-第10行
public class Student2 implements Playable{
    private String name;
    private int no;
    public void sing(String songName) {
        System.out.println(name + " 高声唱 " + songName);
    }
    //这里在Student2类中已经将sing方法定义为了" 高声唱 "
    //通过new Student2（）创建的学生都只能 " 高声唱 "

    public Student2(String name) {
        this.name = name;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }
}
