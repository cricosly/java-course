package com.hidename;

public class Student1 {
    private String name;
    private int no;
    //将Playable作为Student1类的属性
    //通过Student1创建的每一个学生对象
    //sing（）方式并没有被Student1类实现，没有被固定死
    //每个学生有属于自己的独立的Playable作为自己的属性，就像独属于自己的name一样
    //每个学生对象的Playable里sing（）方式可以通过匿名内部类单独重写
    //实现每个学生能各唱各的
    private Playable playable;

    public Student1(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public Playable getPlayable() {
        return playable;
    }

    public void setPlayable(Playable playable) {
        this.playable = playable;
    }
}
