package com.example1;


//子类通过关键词super调用父类
public class Student extends User{
    private int number;

    public Student(String name, int number) {
        //this中没name，需要把name给超类，无法this.name = name;
        //User中name是私有的，无法super.name = name；

        //子类中调用超类的构造函数
        super(name);

        //自己的属性通过this给自己
        this.number = number;
    }

    public int getNumber() {
        super.getName();
        //若直接写getName(),会自动往上找超类中的getName()，不冲突就没问题
        return number;
    }

    @Override//重写Object类中的toString()
    public String toString() {
        return "name: " + super.getName() + "; " + "number: " + getNumber();
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
