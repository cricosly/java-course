package com.example1;

import com.example1.entity.Course;
import com.example1.entity.Student;
import com.example1.entity.Teacher;

import java.awt.print.Book;

public class Test {

    public static void main(String[] args) {
        //courseTest();
        //interfaceTest();
        //enumTest(Season.WINTER);
        Club c1 = new Club();
        //匿名类的使用
        //new的不是learnable接口，是实现了这个接口的匿名类（没名字）
        //基于不同的对象，完成不同的实现
        c1.setLearnable(new Learnable() {
            @Override
            public int test(String courseName) {
                System.out.println(courseName + "匿名test");
                return 10000;
            }

            @Override
            public void read(String bookName) {
                int a = 1 + 2;
                System.out.println(bookName + "匿名read");
                System.out.println("a: " + a);
            }
        });
        System.out.println(c1.getLearnable().test("java"));
        c1.getLearnable().read("C++");


        Course c = new Course();
        //创建了一个匿名子类
        c.setTeacher(new Teacher() {
            public void tet() {

            }
        });
    }

    private static void courseTest() {
        Course c = new Course();
        c.setName("java");
        c.setNumber(1001);

        Teacher t1 = new Teacher();
        t1.setName("80");
        t1.setNumber(110);
        c.setTeacher(t1);

        Student s1 = new Student();
        s1.setName("li");
        //IQ是Student的内部类
        Student.IQ iq1 = new Student.IQ();
        iq1.setDes("good");
        iq1.setPoint(250);
        s1.setIq(iq1);

        Student s2 = new Student();
        s2.setName("zhang");
        s2.setIq(iq1);

        Student[] students = {s1, s2};
        c.setStudents(students);

        listIQ(c);
    }

    //组合的应用
    //打印课程的所有学生的名字
    private static void listNames(Course course) {
        for (Student s : course.getStudents()) {
            System.out.println(s.getName());
        }
    }

    //打印课程的所有学生的IQ值
    private static void listIQ(Course course) {
        for (Student s : course.getStudents()) {
            System.out.println(s.getIq().getPoint());
        }
    }

    //枚举使用
    private static void enumTest(Season season) {
        switch (season) {
            case SPRING:
                System.out.println("1111");
                break;
            case WINTER:
                System.out.println("4444");
                break;
        }
    }

    //接口使用
    private static void interfaceTest() {
        UnderGraduate u1 = new UnderGraduate("Er");
        //符合拥有学习能力的人
        Learnable l1 = new UnderGraduate("BO");
        Learnable l2 = new PostGraduate();
        Playable p = new UnderGraduate("LU");
        p.sing("abab");
        //直接可以实现游泳（在接口中已经将方法具体实现）
        p.swim();
        //通过接口名称可以直接访问其中的静态方法
        Playable.staticTest();

        Club club = new Club();
        club.setName("abed");
        club.setLearnable(l1);
        club.setLearnable(l2);
        club.getLearnable().test("java");
    }

}
