package com.example1;

//研究生
public class PostGraduate implements Learnable{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int test(String courseName) {
        System.out.println("Postgraduate: ");
        return 100;
    }

    public void read(String bookName) {
        System.out.println("本科生read：" + bookName);
    }
}
