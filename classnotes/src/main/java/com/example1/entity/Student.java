package com.example1.entity;

import javax.naming.Name;

public class Student {
    //IQ是Student的内部类,没有源文件，但编译后会有独立的.class文件
    //外部类中的private变量如number无法在内部类IQ里使用
    public static class IQ {
        private int point;
        private String des;

        public int getPoint() {
            return point;
        }

        public void setPoint(int point) {
            this.point = point;
        }

        public String getDes() {
            return des;
        }

        public void setDes(String des) {
            this.des = des;
        }
    }

    private int number;
    private String name;
    private IQ iq;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IQ getIq() {
        return iq;
    }

    public void setIq(IQ iq) {
        this.iq = iq;
    }
}
