package com.example1;

import com.sun.jdi.VMOutOfMemoryException;

public interface Playable {
    void sing(String songName);

    //默认是public
    default void swim() {
        System.out.println("游泳");
        System.out.println("调用私有方法后：");
        prt();
        te();
    }

    //接口可以有常量
    //默认是以下格式
    //public static final 数据类型 常量名称 = 数据值；
    int NUM = 12;
    public final int a = 6;
    public static final int b = 7;
    //不可以申请私有的常量
    // private int a = 1;

    //通过default关键字实现方法
    default void prt1() {
        System.out.println("私有实例方法");
    }

    //私有的非静态方法
    private void prt() {
        System.out.println("私有实例方法");
    }

    //公有静态的方法，默认为public
    static void staticTest() {
        System.out.println("公有静态方法");
    }

    //私有静态方法
    private static void te() {
        System.out.println("私有静态方法");
    }


}
