package com.example1;

public class Club {
    private String name;

    //有学习能力的人
    private Learnable learnable;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Learnable getLearnable() {
        return learnable;
    }

    public void setLearnable(Learnable learnable) {
        this.learnable = learnable;
    }
}

