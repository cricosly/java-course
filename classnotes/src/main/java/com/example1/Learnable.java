package com.example1;

//学习能力的接口
//接口增加的每一个抽象的方法都要在引用了接口的类中添加
//为避免上述情况，java8之后通过default可以在接口中声明方法的具体实现
public interface Learnable {
    //该接口下考试的行为，定义了行为不考虑实现
    //默认为public（公开） abstract（抽象）前缀，可以不写
    //public abstract int test(String courseName);
    int test(String courseName);
    void read(String bookName);

    //接口里定义常量(大写)，默认为public static final前缀，可以不写
    public static final String MAME = "80";
}
