package com.example1;

//本科生
public class UnderGraduate implements Playable, Learnable{
    private String name;

    //构造函数
    public UnderGraduate(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int test(String name) {
        System.out.println("UnderGraduate: ");
        return 0;
    }

    @Override
    public void read(String bookName) {
        System.out.println("本科生read：" + bookName);
    }

    @Override
    public void sing(String songName) {
        System.out.println("本科生sing:" + songName);
    }
    /*不需要添加抽象方法的具体实现
    public void swim() {

    }
    */
    //可以重写接口中具体实现的方法
    @Override//给人看的重写标识
    public void swim() {
        System.out.println("本科生游泳");
    }

}
