package com.example2;

public class Animal implements Movable{
    private String name;

    //Bird的父类Animal的有参构造函数
    public Animal(String name) {
        this.name = name;
    }

    @Override
    public void move() {
        System.out.println("Animal " + name + " is moving");
    }

    protected long getCount() {
        return 10L;
    }

    protected Animal getMovable() {
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //静态方法，子类中也“重写”了barking()，但不叫重写，叫隐藏
    public static void barking() {
        System.out.println("Animal barking");
    }



}
