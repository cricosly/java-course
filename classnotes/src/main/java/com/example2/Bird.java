package com.example2;

public class Bird extends Animal implements Flyable{

    private String color;
    public Bird(String name, String color) {
        //子类Bird必须调用父类Animal的有参构造函数，放在第一行
        super(name);
        //自己的赋给自己
        this.color = color;
        //可以使用父类中定义的方法
        Animal.barking();
        //父类中为public方法，子类可以直接访问
        barking();
    }

    @Override
    public void fly() {
        System.out.println(getName() + "is flying");
    }

    @Override
    public void move() {
        System.out.println("Bird " + getName() + " move so slow");
    }

    @Override
    public long getCount() {
        return 0;
    }

    /*基本数据类型不可更改
    public int getCount() {
        return 0;
    }
     */

    //重写可以改变类型，范围可以扩大，保证方法签名一致
    @Override
    public Bird getMovable() {
        return null;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    //静态的方法不叫重写，叫隐藏超类的静态方法，无需@Override注解
    public static void barking() {
        System.out.println("Bird barking");
    }

}
