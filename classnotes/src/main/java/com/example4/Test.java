package com.example4;

public class Test {
    public static void main(String[] args) {
        //t1();//自动拆装箱
        //t2();//基本数据类型计算效率远高于基于对象计算的效率

    }
    public static void t1() {
        int n1 = 10;
        //自动装箱
        Integer n2 = 12;
        //自动拆箱
        n1 = n2;
        System.out.println(n1);

        //字符串转化成整数
        int a = Integer.valueOf("12");
        System.out.println(a);

        Double d1 = 10.2;
        //会出现精度丢失
        int d = d1.intValue();
        System.out.println(d);
    }

    public static void t2() {
        //纳秒，返回long
        System.nanoTime();
        //毫秒
        System.currentTimeMillis();

        //基本数据类型计算效率远高于基于对象计算的效率

        //拆装箱测试——>速度慢
        long s1 = System.nanoTime();
        //装箱
        Integer i1 = 0;
        for(int i = 0; i < Integer.MAX_VALUE; i++) {
            //装箱，拆箱+1
            i1 = i1 + 1;
        }
        long e1 = System.nanoTime();
        System.out.println(e1 - s1);

        //基本类型测试——>速度快
        long s2 = System.nanoTime();
        int i2 = 0;
        for(int i = 0; i < Integer.MAX_VALUE; i++) {
            i2 = i2 + 1;
        }
        long e2 = System.nanoTime();
        System.out.println(e2 - s2);
    }
}
