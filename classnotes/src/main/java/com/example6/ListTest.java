package com.example6;

import java.util.ArrayList;
import java.util.List;

public class ListTest {
    private static final List<User> USERS = creat();

    private static List<User> creat() {
        User u1 = new User("BO");
        User u2 = new User("SUN");
        User u3 = new User("SUN");
        //new ArrayList<>()创建了0长度的对象数组
        List<User> users = new ArrayList<>();
        users.add(u1);
        users.add(u2);
        users.add(u3);

        return users;
    }
}
