package com.example6;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {

    }
    public static void t0() {
        //基数数值实现的字符串集合
        //ArrayList<>的<>默认里面是String，和前面声明的一致
        List<String> strings = new ArrayList<>();
        //可以塞入字符串
        strings.add("80");
        //不能塞入非字符串类型
        //strings.add(123);
        strings.remove("80");
        //可以迭代，能使用foreach循环语句
        for (String s : strings) {
            System.out.println(s);
        }

        //List<>只能声明引用类型，不能使用基本数据类型
        //List<int> ints = new ArrayList<>();
        List<Integer> ints = new ArrayList<>();

        //集合的元素可以是类
        List<User> users = new ArrayList<>();
        //可以往父类的集合里塞入子类（Student extends User）
        users.add(new Student("s1"));
        //不可以往子类的集合里塞入父类（User extends Object）
        //users.add(new Object());
    }

    public static void t1() {
        User[] users = new User[5];
        Student[] students = new Student[5];
        //数组允许平级的类直接赋值（这是错误的，是一种缺陷）
        users = students;
        System.out.println(students.getClass().getName());
        System.out.println(students.getClass().getSuperclass().getName());
    }


}
