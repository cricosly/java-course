package com.example0;

public class Test {
    public static void main(String[] args) {
        stringUtilsTest();
        stringUtilsTest();
    }

    //静态的，仅首次调用调用的时候初始化一次，二次调用是同一个对象
    private static void stringUtilsTest() {
        System.out.println(StringUtils.pre("word"));
    }
}
