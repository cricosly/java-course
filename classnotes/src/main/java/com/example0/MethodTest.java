package com.example0;

import com.example0.entity.DeepTest;//引用非同级的类作为包，不能引用同名

public class MethodTest {
    public static void main(String[] args) {
        Student s0 = new Student();//用无参的构造函数构造
        s0.setName("000");
        System.out.println("s0成绩:" + s0.getGrade());//自动初始化成绩为1

        Student s1 = new Student("80");
        s1.setNumber(1213);
        System.out.println("s1地址:" + s1);
        System.out.println("s1学号:" + s1.getNumber());
        Student s2 =updateStudentNumber(s1,123456);
        System.out.println("s1学号:" + s1.getNumber());
        System.out.println("s2学号:" + s2.getNumber());
        System.out.println("s2地址:" + s2);

        Student s3 = new Student("80");
        s1.setSex(Student.MALE);
        s2.setSex(Student.FEMALE);
        s3.setSex(Student.FEMALE);
        System.out.println("s3性别:" + s3.getSex());

        DeepTest dt = new DeepTest();
        System.out.println("DeepTest地址:" + dt);
    }
    private static Student updateStudentNumber(Student student, int number){
        student.setNumber(number);
        return student;
    }

}
