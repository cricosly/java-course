package com.example0;

import com.example0.entity.DeepTest;//引用非同级的类作为包，不能引用同名

public class PackageTest {
    public static void main(String[] args) {
        DeepTest t1 = new DeepTest();
        System.out.println(t1);
        System.out.println();
    }
}
