package com.example0;

public class Student {
    static{//静态块，在第一次加载类的时候加载且仅加载一次
        System.out.println("load");
    }
    public static int amount = 0;
    public static final String MALE = "男";//可以直接修改
    public static final String FEMALE = "女";
    private String sex;
    private String name;//其别名有三种：属性/实例成员变量/字段
    private int number;
    private boolean china;
    private int grade;

    public Student (){//默认自动生成无参的构造函数
        this(1);//构造函数中调用构造函数要在最上排
                      //调用无参构造函数后自动调用基于成绩的构造函数，初始化成绩为1
        System.out.println("完成成绩初始化");
        amount++;
    }

    public static int getAmount(){
        return amount;
    }

    public Student(int grade){
        this.grade= grade;
    }

    public Student(String name){
        this.name = name;
    }

    public String getSex(){
        return sex;
    }
    public void setSex(String sex){
        this.sex = sex;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;//将获取的name赋给当前的name
    }

    public int getNumber(){
        return number;
    }
    public void setNumber(int number){
        this.number = number;
    }

    public boolean getChina(){
        return china;
    }
    public void setChina(boolean china){
        this.china = china;
    }

    public int getGrade(){
        return grade;
    }
    public void setGrade(int grade){
        if (grade < 0){
            throw new RuntimeException("error");
        }
        this.grade=grade;
    }
}
