package com.example3;

//抽象的类，abstract修饰
public abstract class Organism {
    private String name;

    //抽象的类不可以实例化（new），构造函数是给子类使用的
    //抽象的类的子类可以仍然是抽象的类，但子类的子类的子类...最后一级子类必须是非抽象的类

    //抽象的类也能有构造函数
    public Organism(String name) {
        this.name = name;
    }

    //具体的public方法，子类可以直接用
    public void sleep() {
        System.out.println(name + "is sleeping");
    }

    //抽象的方法，子类必须重写实现
    public abstract void move();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
