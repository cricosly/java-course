package com.example3;

import java.sql.SQLOutput;

public class Human extends Organism{

    public Human(String name) {
        super(name);
    }

    @Override
    //抽象的类中的抽象的方法，子类必须重写实现
    public void move() {
        System.out.println(getName() + " move as human");
    }

    public void humanSleep() {
        //子类可以直接用抽象的类中具体的public方法
        sleep();
    }
}
