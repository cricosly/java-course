package com.hideclass;

public class Test {
    public static void main(String[] args) {
        //把Playable作为属性的Student1类
        //通过匿名内部类重构唱的方式
        //每个学生唱的方式不一样
        Student1 s11 = new Student1("s11");
        s11.setPlayable(new Playable() {
            @Override
            public void sing(String songName) {
                System.out.println("s11 站着唱 " + songName);
            }
        });
        s11.getPlayable().sing("1号歌");

        Student1 s12 = new Student1("s12");
        s11.setPlayable(new Playable() {
            @Override
            public void sing(String songName) {
                System.out.println("s12 坐着唱 " + songName);
            }
        });
        s11.getPlayable().sing("2号歌");

        System.out.println("*********************");

        //直接接入Playable接口的Student2类
        //由于没有将Playable作为属性，无法构造匿名内部类
        //所有学生唱的方式固定成一样
        Student2 s21 = new Student2("s21");
        s21.sing("3号歌");
        Student2 s22 = new Student2("s22");
        s21.sing("4号歌");

    }


}
