package com.hideclass;

public interface Playable {
    //定义了一个抽象的方法“唱歌”
    //接入了这个“Playable”接口的类都必须将这个方法具体实现
    void sing(String songName);
}
