package com.experiment04.service.impl;

import com.experiment04.entity.Student;
import com.experiment04.resource.DatabaseUtils;
import com.experiment04.service.StudentService;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StudentServiceImpl implements StudentService {

    //调用DatabaseUtils获取集合对象
    private final List<Student> students = DatabaseUtils.listStudents();

    /**
     * 1.向集合添加一名学生，返回当前全部学生
     * @param student
     * @return 学生集合
     */
    @Override
    public List<Student> addStudent(Student student) {
        students.add(student);
        return students;
    }

    /**
     * 2.列出指定年级的全部学生
     * @param year
     * @return 学生集合
     */
    @Override
    public List<Student> listStudentsByYear(int year) {
        return DatabaseUtils.listStudents().stream()
                //过滤
                .filter(s -> s.getYear() == year)
                //聚合
                //.collect(Collectors.toList());
                .toList();
    }

    /**
     * 3.列出指定年级，指定性别的全部学生的姓名
     * @param year
     * @param sex
     * @return 名字集合
     */
    @Override
    public List<String> listStudentsNames(int year, Student.Sex sex) {
        return students.stream()
                //过滤
                .filter(s -> s.getSex() == sex)
                .filter(s -> s.getYear() == year)
                //映射新类型——>姓名,简化写法
                .map(Student::getName)
                //聚合，简化写法
                //.collect(Collectors.toList())
                .toList();

    }

    /**
     * 4.列出指定年级的全部学生，按学生ID逆序排列
     * @param year
     * @return 学生集合
     */
    @Override
    public List<Student> listStudentsByYearSortedById(int year) {
        return students.stream()
                .filter(s -> s.getYear() == year)
                //逆序排列
                .sorted(Comparator.comparing(Student::getId).reversed())
                .toList();
    }

    /**
     * 5.将所有学生，以性别分组
     * @return Map,键为性别，值为学生集合
     */
    @Override
    public Map<Student.Sex, List<Student>> listStudentsMapBySex() {
        return students.stream()
                //groupingBy()仅传入一个参数时
                //自动将分组后的元素存入一个List
                //最终返回的结果是一个Map对象
                //其中键为性别，值为该性别对应的学生集合
                .collect(Collectors.groupingBy(Student::getSex));
    }

    /**
     * 6.列出指定年级学生，按学号分组
     * @return Map，键为学号，值为学生
     */
    @Override
    public Map<Integer, Student> listStudentsByYearMapById(int year) {
        return students.stream()
                //过滤指定年级学生
                .filter(s -> s.getYear() == year)
                //学号为键，学生为值
                .collect(Collectors.toMap(Student::getId, s -> s));
    }

    /**
     * 7.移除指定学号的学生，返回是否成功移除
     * @param id
     * @return
     */
    @Override
    public boolean removeStudent(int id) {
        return students.removeIf(s -> s.getId() == id);
    }
}