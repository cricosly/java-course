package com.experiment04;

import com.experiment04.entity.Student;
import com.experiment04.service.StudentService;
import com.experiment04.service.impl.StudentServiceImpl;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Test {
    public static final StudentServiceImpl studentService = new StudentServiceImpl();

    public static void main(String[] args) {
        Student student = new Student(202122, Student.Sex.FEMALE, "瑞瑞", 2021);
        testAddStudent(student);
        System.out.println();
        testListStudentsByYear(2010);
        System.out.println();
        testListStudentsNames(2010, Student.Sex.MALE);
        System.out.println();
        testListStudentsByYearSortedById(2010);
        System.out.println();
        testListStudentsMapBySex();
        System.out.println();
        testListStudentsByYearMapById(2010);
        System.out.println();
        testRemoveStudent(201015);
    }

    private static void testAddStudent(Student student) {
        System.out.println("1.向集合添加一名学生，返回当前全部学生");
        List<Student> students = studentService.addStudent(student);
        //输出添加了一名学生后集合中的人数
        System.out.println("添加后有学生" + students.size() + "人");
        //输出每个学生
        for(Student s : students) {
            System.out.println(s.getId() + " " + s.getSex() + " " + s.getName() + " " + s.getYear());
        }
    }

    private static void testListStudentsByYear(int year) {
        System.out.println("2.列出指定年级的全部学生");
        List<Student> students = studentService.listStudentsByYear(year);
        //输出每个学生
        for(Student s : students) {
            System.out.println(s.getId() + " " + s.getSex() + " " + s.getName() + " " + s.getYear());
        }
    }

    private static void testListStudentsNames(int year, Student.Sex sex) {
        System.out.println("3.列出指定年级，指定性别的全部学生的姓名");
        List<String> names = studentService.listStudentsNames(year, sex);
        //输出每个学生的名字
        for(String name : names) {
            System.out.println(name);
        }
    }

    private static void testListStudentsByYearSortedById(int year) {
        System.out.println("4.列出指定年级的全部学生，按学生ID逆序排列");
        List<Student> students = studentService.listStudentsByYearSortedById(year);
        //输出每个学生
        for(Student s : students) {
            System.out.println(s.getId() + " " + s.getSex() + " " + s.getName() + " " + s.getYear());
        }
    }

    private static void testListStudentsMapBySex() {
        System.out.println("5.将所有学生，以性别分组");
        //键为性别，值为学生集合
        Map<Student.Sex, List<Student>> map = studentService.listStudentsMapBySex();
        //使用Map的entrySet()方法将Map转换为Set集合
        //遍历Set集合中的每个元素输出键值对
        Set<Map.Entry<Student.Sex, List<Student>>> sets = map.entrySet();
        for(Map.Entry<Student.Sex, List<Student>> set : sets) {
            Student.Sex sex = set.getKey();
            //输出键——>性别
            System.out.println(sex);
            List<Student> studentList = set.getValue();
            //输出值——>学生集合
            for(Student s : studentList) {
                System.out.println(s.getId() + " " + s.getSex() + " " + s.getName() + " " + s.getYear());
            }
        }
    }

    private static void testListStudentsByYearMapById(int year) {
        System.out.println("6.列出指定年级学生，按学号分组");
        //键为学号，值为学生
        Map<Integer, Student> map = studentService.listStudentsByYearMapById(year);
        //使用Map的entrySet()方法将Map转换为Set集合
        //遍历Set集合中的每个元素输出键值对
        Set<Map.Entry<Integer, Student>> sets = map.entrySet();
        for(Map.Entry<Integer, Student> set : sets) {
            int id = set.getKey();
            //输出键——>学号
            System.out.println(id);
            Student s = set.getValue();
            //输出值——>学生
            System.out.println(s.getId() + " " + s.getSex() + " " + s.getName() + " " + s.getYear());
        }
    }

    private static void testRemoveStudent(int idRemove) {
        System.out.println("7.移除指定学号的学生，返回是否成功移除");
        System.out.print("移除前:");
        //这里移除的是学号为201015的学生
        testListStudentsByYear(2010);
        boolean result = studentService.removeStudent(idRemove);
        System.out.println("remove " + idRemove + ": " + result);
        System.out.print("移除后:");
        testListStudentsByYear(2010);
    }
}