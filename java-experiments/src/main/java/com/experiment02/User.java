package com.experiment02;

//用户
public class User {
    //名字
    private String userName;
    //余额
    private int userMoney;
    //地址
    private String address;

    //全参数构造函数
    public User(String userName, int userMoney, String address) {
        this.userName = userName;
        this.userMoney = userMoney;
        this.address = address;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserMoney() {
        return userMoney;
    }

    public void setUserMoney(int userMoney) {
        this.userMoney = userMoney;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
