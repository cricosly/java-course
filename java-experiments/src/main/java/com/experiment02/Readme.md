## 实验二 类的创建与封装实验
### 实验目的
掌握实体类的声明定义方法  
掌握类构造函数的声明定义方法  
掌握类实例级成员变量声明定义方法  
掌握类实例级成员变量访问器(getter/setter)的声明方法  
掌握对象声明与使用方法  
掌握各种访问级别的作用范围     
掌握组合设计方法

### 实验内容
创建实验项目工程java-experiments  
设计用户实体类User，包含用户姓名，账户余额，地址；全属性构造函数  
设计商品实体类Product，包含商品名称，商品价格，全属性构造函数  
*由于同一商品可购买多个，设计购买项实体类Item，包含购买商品，商品的购买数量，全属性构造函数  
*设计订单实体类Order，包含本次订单的用户，购买的全部商品项，本次购买总花费(后续商品价格的改变不应对本次已购买数据产生影响)，全属性构造函数    
实体类应包含属性对应的按命名约定编写的getter/setter方法

设计订单业务逻辑处理类OrderService，提供静态方法addOrder()，实现：  
传入购买的用户，以及本次购买的全部购买项   
当用户余额大于购买商品总花费时，创建订单，封装购买用户/购买项/总花费，并返回订单对象  
当余额小于总花费时，返回空引用  
此业务方法的返回类型？参数？实现具体业务逻辑

创建测试类及主函数，模拟1个用户，2件商品，购买的商品及数量，调用订单逻辑方法测试。  
当订单创建成功时，打印商品总额，用户余额，快递地址；当订单创建失败时，打印用户余额不足  
测试数据可忽略精度丢失


### 一、何为封装？
> 封装，即隐藏对象的**属性**和**实现细节**，仅对外公开接口/方法，控制在程序中属性的  
> 读和修改的访问级别；将抽象得到的数据和行为（或功能）相结合，形成一个有机  
> 的整体，也就是将数据与操作数据的源代码进行有机的结合，形成“类”，其中数据  
> 和函数都是类的成员。把类中的某些信息进行隐藏，从而使外部程序不能直接对这  
> 些信息进行直接的访问，只能通过类中定义的方法对这些隐藏的信息进行操作和访问。  

把类中的某些信息进行隐藏，从而使外部程序不能直接对这些信息进行直接的访问，  
只能通过类中定义的方法对这些隐藏的信息进行操作和访问。

### 二、封装的好处
- 隐藏信息和实现细节，保证数据与代码的安全性
- 重复调用，避免代码冗余，程序编写效率高
- 能对成员访问权限就行合理控制
- 让代码更容易理解和维护

### 三、构造函数
类的构造函数，像是一个方法，没有返回值（void都没有），方法名与类名一致  
- 无参构造函数  
当类中未显式声明构造函数时编译器自动创建无参的构造函数
```
public class Student {
    private String name;
    
    public Student() {
    
    }
```

- 有参构造函数  
当类中显式声明了构造函数后，编译器不再自动创建无参的构造函数  
this表示当前对象的引用，通过this可以在实例方法或构造函数中引用当前对象的成员变量
```
public class Student {
    private String name;

    public Student(String name) {
        //传入的参数赋值给此对象的属性
        this.name = name;
    }
```

- 多个构造函数  
一个类中可以声明多个构造函数，可以基于不同的构造函数创建对象并初始化属性值  
各个构造函数的方法签名不能一样  
构造函数可以通过this关键词调用同一个类中的其他构造函数，调用语句必须在第一行
```
public class Student {
    private String name;
    private int grade;
    
    public Student () {
        //构造函数中调用其他构造函数要在最上排
        //调用无参构造函数后自动调用基于成绩的构造函数，初始化成绩为1
        this(1);            
        System.out.println("完成成绩初始化");
    }
    public Student(int grade) {
        this.grade= grade;
    }
    public Student(String name) {
        this.name = name;
    }
```

### 四、访问器(getter/setter)的声明
> 为了保护属性的数据安全，使用了private访问权限修饰符，限制了只能在类中使用。  
> 那么对于外部的对象，如何给属性进行操作？  
- 取值，使用**getter**方法  
```
public 成员变量的类型 get成员变量名() {
   return 成员变量名;
}

例如：
public String getName(){
   return name;
}
```

- 赋值，使用**setter**方法
```
public void set成员变量名(形式参数){
   //给成员变量进行赋值
   this.属性 = 参数名;
}

例如：
public void setName(String name){
   this.name = name;
}
```


***
---
_________________
