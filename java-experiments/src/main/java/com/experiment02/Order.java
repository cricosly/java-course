package com.experiment02;

//订单
public class Order {
    private User user;
    private Item[] items;
    //将购买总花费作为订单实体类Order的一个属性
    private int cost;

    public Order(User user, Item[] items, int cost) {
        this.user = user;
        this.items = items;
        this.cost = cost;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Item[] getItems() {
        return items;
    }

    public void setItems(Item[] items) {
        this.items = items;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
