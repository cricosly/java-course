package com.experiment02;

public class Test {
    public static void main(String[] args) {
        int cost = 0;
        User u = new User("u1", 100, "道外");

        Product p1 = new Product("铅笔", 10);
        Product p2 = new Product("橡皮", 5);

        Item i1 = new Item(p1,9);
        Item i2 = new Item(p2,2);
        Item[] items = {i1,i2};

        //生成订单
        Order order = OrderService.addOrder(u, items);

        if(order != null) {
            System.out.println("商品总额: " + order.getCost());
            System.out.println("用户余额: " + u.getUserMoney());
            System.out.println("快递地址: " + u.getAddress());
        }
        else {
            System.out.println("用户余额不足");
        }

    }
}
