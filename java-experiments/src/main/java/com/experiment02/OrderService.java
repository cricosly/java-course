package com.experiment02;

//点单服务
public class OrderService {

    public static Order addOrder(User user, Item[] items) {
        int cost = 0;
        /*
        for (int i = 0; i < items.length; i++) {
            cost = cost + items[i].getProduct().getPrice() * items[i].getNumber();
        }
         */
        //购买总花费
        for (Item item : items) {
            cost += item.getNumber() * item.getProduct().getPrice();
        }

        if (user.getUserMoney() >= cost) {
            //创建订单
            Order order = new Order(user, items, cost);
            //扣除用户余额
            user.setUserMoney(user.getUserMoney() - cost);
            return order;
        }
        //用户余额不足，返回空引用
        else {
            return null;
        }
    }
}
