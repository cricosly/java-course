package com.experiment01.otherTest;

import java.util.Scanner;

public class Test3 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);//创建一个 Scanner 对象，等待输入

        //输入字符串前输入一个整数
        System.out.println("请输入整数：");
        int z = in.nextInt();
        System.out.println(z);

        //吃到回车
        in.nextLine();

        System.out.println("请输入字符串1：");
        //带空格的字符串：
        String a = in.nextLine();
        System.out.println(a);

        System.out.println("请输入字符串2：");
        //不带空格的字符串：
        String b = in.next();
        System.out.println(b);

    }
}
