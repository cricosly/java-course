package com.experiment01.otherTest;

public class Test2 {
    public static void main(String[] args) {

        //负数的小数次方，返回NaN（不是一个数字）
        double a =  Math.pow(-1, 0.5);
        System.out.println("1:" + a);

        //0的负数次方，相当于0作分母，返回Infinity (无穷)
        double b =  Math.pow(0, -1.3);
        System.out.println("2:" + b);

        //返回值超出double的上限，返回Infinity (无穷)
        double c =  Math.pow(2, 1024);
        System.out.println("3:" + c);

        //Math.pow(x,y) 的计算结果返回是double类型，其他类型需要进行类型转化
        int d = (int) Math.pow(2, 1023);
        System.out.println("4:" + d);

    }
}
