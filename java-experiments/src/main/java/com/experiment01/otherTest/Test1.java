package com.experiment01.otherTest;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Test1 {
    public static void main(String[] args) {
        Test1 t1 =new Test1();
        t1.t1();
    }
    public void t1() {
        //在本类中，非静态方法直接调用静态方法
        t2();
        //在本类中，非静态方法直接调用非静态方法
        t3();
        //在不同类中,非静态方法通过类名.方法()调用不同类静态方法
        Test12.t4();
        //在不同类中,非静态方法通过创建对象调用不同类非静态方法
        Test12 t2 = new Test12();
        t2.t5();
    }
    public static void t2() {
        System.out.println("t2");
    }
    public void t3() {
        System.out.println("t3");
    }
}
class Test12 {
    public static void t4() {
        System.out.println("t4");
    }
    public void t5() {
        System.out.println("t5");
    }
}



class GFG {

    public static void main(String[] args)
    {
        // Creating an integer array
        int arr[] = { 1, 2, 3, 4, 5 };

        // Using Arrays.stream() to convert
        // array into Stream
        IntStream stream = Arrays.stream(arr);
        // Displaying elements in Stream
        stream.forEach(str -> System.out.print(str + " "));
    }
}

class Test13 {
    private int i = 20;
    public void fuc(int i) {
        this.i = i * 2;
        System.out.println(this.i);
    }
    public static void main(String[] args) {
        System.out.println(StringUtils.NAME);
        Test13 t = new Test13();
        t.fuc(1);
    }
}
