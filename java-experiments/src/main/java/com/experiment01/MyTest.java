package com.experiment01;

import java.util.Scanner;
import java.util.Arrays;

public class MyTest {
    public static void main(String[] args) {
        System.out.println("闰年计算：");
        leapYearsTest(1899, 2046);
        System.out.println("冒泡排序1：");
        bubbleSortTest1();
        System.out.println("冒泡排序2：");
        bubbleSortTest2();
        System.out.println("冒泡排序3：");
        bubbleSortTest3();
        System.out.println("冒泡排序4：");
        bubbleSortTest4();
        System.out.println("求a+aa+aaa+aa…a(n个a)之和，a=3");
        System.out.println("请输入一个正整数repeat：");
        Scanner in = new Scanner(System.in);//创建一个 Scanner 对象，等待输入
        int repeat = in.nextInt();
        summationTest(repeat);
    }

    //闰年计算，通过开始/结束2个年份，输出年份范围内的闰年
    private static void leapYearsTest(int startYear, int endYear) {
        for (int i = startYear; i <= endYear; i++) {
            if (i % 400 == 0 || (i % 4 == 0 && i % 100 != 0)) {
                System.out.println(i);
            }
        }
    }

    // 冒泡排序1:数据结构优化写法
    // 对比相邻2个数，得到最小数
    // 加一个标记，如果那一趟排序没有交换元素
    // 说明这组数据已经有序了，自动跳出循环
    public static void bubbleSortTest1() {
        int[] ints = {8, 3, 4, 22, 1};
        int m = ints.length - 1;
        int flag = 1;
        while(m >= 0 && flag == 1)
        {
            flag = 0;
            // m = length-1
            // 每次让 ints[i] 与 ints[i+1] 比较
            // i从 0 递增到 length-1 ，防止数组越界
            for(int i = 0; i < m; i++)
                //小的数置换到前面
                if(ints[i] > ints[i + 1])
                {
                    flag = 1;
                    int temp = ints[i + 1];
                    ints[i + 1] = ints[i];
                    ints[i] = temp;
                }
            m--;
        }
        for (int i : ints) {
            System.out.println(i + " ");
        }
    }

    // 冒泡排序2:经典写法
    // 取一个位置上的数，分别和其他数做对比,确定该位置的准确数
    private static void bubbleSortTest2() {
        int[] ints = {8, 3, 4, 22, 1};
        // 从第1个元素开始，到倒数第二个元素
        for (int i = 0; i < ints.length - 1; i++) {
            // 从当前元素的下一个元素开始，到最后一个元素
            for (int j = i + 1; j < ints.length; j++) {
                // 比较，小的数置换到前面
                if (ints[i] > ints[j]) {
                    int temp = ints[j];
                    ints[j] = ints[i];
                    ints[i] = temp;
                }
            }
        }
        for (int i : ints) {
            System.out.println(i);
        }
    }

    // 冒泡排序3：
    private static void bubbleSortTest3() {
        int[] ints = {8, 3, 4, 22, 1};
        for (int i = 0; i < ints.length - 1; i++) {
            for (int j = i + 1; j < ints.length; j++) {
                // 无需置换，但需要比较2次
                int min = Math.min(ints[i], ints[j]);
                int max = Math.max(ints[i], ints[j]);
                ints[i] = min;
                ints[j] = max;
            }
        }
        for (int i : ints) {
            System.out.println(i);
        }
    }

    //冒泡排序4：集合流封装的排序操作
    private static void bubbleSortTest4() {
        int[] ints = {8, 3, 4, 22, 1};
        Arrays.stream(ints)
                .sorted()
                .forEach(System.out::println);
    }

    // 求a+aa+aaa+aa…a(n个a)之和
    private static void summationTest(int repeat) {
            // 数字
            double a = 3;
            double current = 0;
            double result = 0;
            for (int i = 0; i < repeat; i++) {
                current = current + a * Math.pow(10, i);
                result += current;
            }
            System.out.println(result);
    }

}


