package com.experiment01;

public class BubbleSortTest {
    public static void main(String[] args) {
        bubbleSortTest1();
    }

    public static void bubbleSortTest1() {
        int[] ints = {8, 3, 4, 22, 1};
        int m = ints.length - 1;
        int flag = 1;
        while(m >= 0 && flag == 1)
        {
            flag = 0;
            //m=length-1
            //每次让ints[i] 与 ints[i+1]比较
            //i从0递增到length-1，防止数组越界
            for(int i = 0; i < m; i++)
                if(ints[i] > ints[i + 1])
                {
                    flag = 1;
                    int temp = ints[i + 1];
                    ints[i + 1] = ints[i];
                    ints[i] = temp;
                }
            m--;
        }
        for (int i : ints) {
            System.out.println(i);
        }
    }
}

