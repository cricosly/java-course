package com.experiment01;

public class LeapYearsTest {
    public static void leapYearsTest(int startYear, int endYear) {
        for (int i = startYear; i <= endYear; i++) {
            if (i % 400 == 0 || (i % 4 == 0 && i % 100 != 0)) {
                System.out.println(i);
            }
        }
    }
}