package com.experiment06;

//自定义受检异常RuntimeException
public class LoadException extends Exception {
    public LoadException(String message) {
        super(message);
    }
}

