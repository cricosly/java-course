package com.experiment06;

import com.experiment06.entity.Container;
import com.experiment06.entity.Ship;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        Ship ship = new Ship(1, "Java 号", 100.0);
        List<Container> containers = new ArrayList<>();
        containers.add(new Container(1, 30.0));
        containers.add(new Container(2, 40.0));
        containers.add(new Container(3, 50.0));

        try {
            LoadShipService.loadShip(ship, containers);
            System.out.println("Ship name: " + ship.getName());
        } catch (LoadException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("启航");
        }
    }
}
