package com.experiment06;

import com.experiment06.entity.Container;
import com.experiment06.entity.Ship;

import java.util.List;

//装载业务逻辑类
public class LoadShipService {
    //装载业务逻辑方法，出现异常不自行处理，而是抛出自定义异常，由调用者捕获处理
    public static void loadShip(Ship ship, List<Container> containers) throws LoadException {
        double allWeight = 0.0;
        for (Container container : containers) {
            allWeight += container.getWeight();
        }
        if (allWeight > ship.getWeight()) {
            //传入异常信息并抛出
            throw new LoadException("Ship ID: "
                    + ship.getId()
                    + ", Name: "
                    + ship.getName()
                    + ", 超重了 "
                    + (allWeight - ship.getWeight()));
        }

        ship.setContainers(containers);
    }
}

