package com.experiment03;

//经理实体类，经理也是一类员工
public class Manager extends Employee {
    //经理级别
    private int managerLevel;

    public Manager(double salary, int managerLevel) {
        super(salary);
        this.managerLevel = managerLevel;
    }

    @Override//重写经理的工资计算方法
    public double calculateSalary() {
        return super.calculateSalary() * managerLevel * 0.8;
    }

}
