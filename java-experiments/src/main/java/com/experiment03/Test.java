package com.experiment03;

public class Test {
    public static void main(String[] args) {
        Manager m = new Manager(5000,2);
        Seller s = new Seller(3000,20000,0.15);
        Employee[] employees = {m, s};
        System.out.println(SalaryService.getTotalSalaries(employees));
    }
}
