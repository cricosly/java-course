package com.experiment03;

//薪资业务逻辑操作类
public class SalaryService {
    public static double getTotalSalaries(Employee[] employees) {
        //所有员工总工资
        double allSalary = 0;

        for (Employee employee : employees) {
            allSalary += employee.calculateSalary();
        }

        return allSalary;
    }
}
