package com.experiment03;

//员工实体类
public class Employee implements Workable {
    private String name;
    private double Salary;

    public Employee(double salary) {
        this.Salary = salary;
    }

    @Override
    public void updateName(String name) {
        this.name = name;
    }

    @Override
    public double calculateSalary() {
        return Salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
