package com.experiment03;

//销售实体类,销售也是一类员工
public class Seller extends Employee {
    //销售额
    private double salesVolume;
    //销售提成
    private double salesCommission;

    public Seller(double salary, double salesVolume, double salesCommission) {
        super(salary);
        this.salesVolume = salesVolume;
        this.salesCommission = salesCommission;
    }

    @Override//重写销售的工资计算方法
    public double calculateSalary() {
        return super.calculateSalary() + salesVolume * salesCommission;
    }
}
