package com.experiment07.entity;

public class Ticket {
    private Integer id;
    //起点站
    private String sta;
    //终点站
    private String end;

    public Ticket(Integer id, String sta, String end) {
        this.id = id;
        this.sta = sta;
        this.end = end;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSta() {
        return sta;
    }

    public void setSta(String sta) {
        this.sta = sta;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
