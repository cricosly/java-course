package com.experiment07.entity;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

//售票点
public class Outlet {
    private String name;
    private List<Ticket> tickets = new LinkedList<>();
    //静态，每一个售票点都需要随机休眠一下
    private static final Random random = new Random();

    public Outlet(String name) {
        this.name = name;
    }

    public synchronized void sell() {
        Ticket ticket = null;
        try {
            //模拟随机50毫秒内售票时核实身份等的其他业务逻辑操作(即sleep)
            Thread.sleep(random.nextInt(50));
            ticket = Department.getTicket();
            if(ticket != null) {
                //售票数+1
                tickets.add(ticket);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        //从铁道部获取票并卖出
    }

    public int getCount() {
        return tickets.size();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
