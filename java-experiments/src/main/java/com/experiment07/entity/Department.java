package com.experiment07.entity;

import java.util.LinkedList;
import java.util.List;

//铁道部
public class Department {
    private static final List<Ticket> tickets = create();

    // 铁道部如何印票无需暴露出去
    private static List<Ticket> create() {
        // 卖出一张票，移除头部一张票，ArrayList每次都要重新创建数组
        // 用LinkedList效率更高
        List<Ticket> tickets = new LinkedList<>();

        for (int i = 1; i <= 200; i++) {
            tickets.add(new Ticket(i, "哈尔滨", "北京"));
        }
        return tickets;
    }

    // 铁道部就一个，不需要创建多个铁道部对象
    // 故获取票的方法静态即可,synchronized设置为同步方法
    public static synchronized Ticket getTicket() {
        Ticket ticket = null;
        if (tickets.size() > 0) {
            //删除指定索引位置上的元素，并返回该元素
            ticket = tickets.remove(0);
        }
        return ticket;
    }
}
