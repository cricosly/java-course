package com.experiment07;

import com.experiment07.entity.Outlet;

import java.util.concurrent.CountDownLatch;

//测试时创建使用多线程
public class Test {
    public static void main(String[] args) throws InterruptedException {
        Outlet o1 = new Outlet("A");
        Outlet o2 = new Outlet("B");
        Outlet o3 = new Outlet("C");

        //总共200张票，三个售票点
        //600个人，分别到三个售票点抢这200张票
        int count = 600;
        CountDownLatch latch = new CountDownLatch(count);

        //每个售票点200个人去抢票
        for (int i = 0; i < 200; i++) {
            new Thread(()->{
                o1.sell();
                latch.countDown();
            }).start();
        }

        for (int i = 0; i < 200; i++) {
            new Thread(()->{
                o2.sell();
                latch.countDown();
            }).start();
        }

        for (int i = 0; i < 200; i++) {
            new Thread(()->{
                o3.sell();
                latch.countDown();
            }).start();
        }

        //主线程等待直到卖完,异常直接抛出
        latch.await();
        System.out.println(o1.getName() + "售票数：" + o1.getCount());
        System.out.println(o2.getName() + "售票数：" + o2.getCount());
        System.out.println(o3.getName() + "售票数：" + o3.getCount());

    }
}
